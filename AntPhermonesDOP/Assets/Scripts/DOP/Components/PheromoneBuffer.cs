﻿using Unity.Entities;

namespace DOP
{
    public struct PheromoneBuffer : IBufferElementData
    {
        public float strength;
    }
}