﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

[StructLayout(LayoutKind.Sequential)]
public class Ant: MonoBehaviour
{
    public float mapScale = 1f;
    public AntManager AntManager => AntManager.Instance;

    public static Matrix4x4[] rotationMatrixLookup;
    public static int rotationResolution = 360;

    public Vector2 position; //8bytes

    public float facingAngle, brightness;
    public float speed, trailAddSpeed, antSpeed, antAccel;
    public float randomSteering, pheromoneSteerStrength, wallSteerStrength, goalSteerStrength;
    // 40bytes
    public bool holdingResource; //4byte
    public Color redColor, yellowColor;
    public void SetUp(Vector2 pos)
    {
        position = pos;
        facingAngle = Random.value * Mathf.PI * 2f;
        speed = 0f;
        holdingResource = false;
        brightness = Random.Range(.75f, 1.25f);
        transform.position = position * mapScale;
    }

    float PheromoneSteering(float distance)
    {
        float output = 0;

        for (int i = -1; i <= 1; i += 2)
        {
            float angle = facingAngle + i * Mathf.PI * .25f;
            float testX = position.x + Mathf.Cos(angle) * distance;
            float testY = position.y + Mathf.Sin(angle) * distance;

            if (testX < 0 || testY < 0 || testX >= AntManager.mapSize || testY >= AntManager.mapSize)
            {

            }
            else
            {
                int index = PheromoneIndex((int)testX, (int)testY);
                float value = AntManager.pheromones[index].r;
                output += value * i;
            }
        }
        return Mathf.Sign(output);
    }
    int PheromoneIndex(int x, int y)
    {
        return x + y * AntManager.mapSize;
    }
    int WallSteering(float distance)
    {
        int output = 0;

        for (int i = -1; i <= 1; i += 2)
        {
            float angle = facingAngle + i * Mathf.PI * .25f;
            float testX = position.x + Mathf.Cos(angle) * distance;
            float testY = position.y + Mathf.Sin(angle) * distance;

            if (testX < 0 || testY < 0 || testX >= AntManager.mapSize || testY >= AntManager.mapSize)
            {

            }
            else
            {
                int value = AntManager.GetObstacleBucket(testX, testY).Length;
                if (value > 0)
                {
                    output -= i;
                }
            }
        }
        return output;
    }
    Matrix4x4 GetRotationMatrix(float angle)
    {
        angle /= Mathf.PI * 2f;
        angle -= Mathf.Floor(angle);
        angle *= rotationResolution;
        return rotationMatrixLookup[((int)angle) % rotationResolution];
    }

    bool Linecast(Vector2 point1, Vector2 point2)
    {
        float dx = point2.x - point1.x;
        float dy = point2.y - point1.y;
        float dist = Mathf.Sqrt(dx * dx + dy * dy);

        int stepCount = Mathf.CeilToInt(dist * .5f);
        for (int i = 0; i < stepCount; i++)
        {
            float t = (float)i / stepCount;
            if (AntManager.GetObstacleBucket(point1.x + dx * t, point1.y + dy * t).Length > 0)
            {
                return true;
            }
        }

        return false;
    }

    void DropPheromones(float strength)
    {
        int x = Mathf.FloorToInt(position.x);
        int y = Mathf.FloorToInt(position.y);
        if (x < 0 || y < 0 || x >= AntManager.mapSize || y >= AntManager.mapSize)
        {
            return;
        }

        int index = PheromoneIndex(x, y);
        AntManager.pheromones[index].r += (trailAddSpeed * strength * Time.fixedDeltaTime) * (1f - AntManager.pheromones[index].r);
        if (AntManager.pheromones[index].r > 1f)
        {
            AntManager.pheromones[index].r = 1f;
        }
    }

    public void AntFixedUpdate(int i, int instancesPerBatch)
    {
        float targetSpeed = antSpeed;
        facingAngle += Random.Range(-randomSteering, randomSteering);

        float pheroSteering = PheromoneSteering(3f);
        int wallSteering = WallSteering(1.5f);
        facingAngle += pheroSteering * pheromoneSteerStrength;
        facingAngle += wallSteering * wallSteerStrength;

        targetSpeed *= 1f - (Mathf.Abs(pheroSteering) + Mathf.Abs(wallSteering)) / 3f;

        speed += (targetSpeed - speed) * antAccel;

        //Debug.LogError(targetSpeed);

        Vector2 targetPos;
        int index1 = i / instancesPerBatch;
        int index2 = i % instancesPerBatch;

        if (holdingResource == false)
        {
            targetPos = AntManager.resourcePosition;

            AntManager.antColors[index1][index2] += ((Vector4)AntManager.searchColor * brightness - AntManager.antColors[index1][index2]) * .05f;
        }
        else
        {
            targetPos = AntManager.colonyPosition;
            AntManager.antColors[index1][index2] += ((Vector4)AntManager.carryColor * brightness - AntManager.antColors[index1][index2]) * .05f;
        }

        if (Linecast(position, targetPos) == false)
        {
            Color color = Color.green;
            float targetAngle = Mathf.Atan2(targetPos.y - position.y, targetPos.x - position.x);
            if (targetAngle - facingAngle > Mathf.PI)
            {
                facingAngle += Mathf.PI * 2f;
                color = Color.red;
            }
            else if (targetAngle - facingAngle < -Mathf.PI)
            {
                facingAngle -= Mathf.PI * 2f;
                color = Color.red;
            }
            else
            {
                if (Mathf.Abs(targetAngle - facingAngle) < Mathf.PI * .5f)
                    facingAngle += (targetAngle - facingAngle) * goalSteerStrength;
            }
        }
        if ((position - targetPos).sqrMagnitude < 4f * 4f)
        {
            holdingResource = !holdingResource;
            facingAngle += Mathf.PI;
        }

        float vx = Mathf.Cos(facingAngle) * speed;
        float vy = Mathf.Sin(facingAngle) * speed;
        float ovx = vx;
        float ovy = vy;

        if (position.x + vx < 0f || position.x + vx > AntManager.mapSize)
        {
            vx = -vx;
        }
        else
        {
            position.x += vx;
        }
        if (position.y + vy < 0f || position.y + vy > AntManager.mapSize)
        {
            vy = -vy;
        }
        else
        {
            position.y += vy;
        }

        float dx, dy, dist;

        Obstacle[] nearbyObstacles = AntManager.GetObstacleBucket(position);
        for (int j = 0; j < nearbyObstacles.Length; j++)
        {
            Obstacle obstacle = nearbyObstacles[j];
            dx = position.x - obstacle.position.x;
            dy = position.y - obstacle.position.y;
            float sqrDist = dx * dx + dy * dy;
            if (sqrDist < AntManager.obstacleRadius * AntManager.obstacleRadius)
            {
                dist = Mathf.Sqrt(sqrDist);
                dx /= dist;
                dy /= dist;
                position.x = obstacle.position.x + dx * AntManager.obstacleRadius;
                position.y = obstacle.position.y + dy * AntManager.obstacleRadius;

                vx -= dx * (dx * vx + dy * vy) * 1.5f;
                vy -= dy * (dx * vx + dy * vy) * 1.5f;
            }
        }

  

        float inwardOrOutward = -AntManager.outwardStrength;
        float pushRadius = AntManager.mapSize * .4f;
        if (holdingResource)
        {
            inwardOrOutward = AntManager.inwardStrength;
            pushRadius = AntManager.mapSize;
        }
        dx = AntManager.colonyPosition.x - position.x;
        dy = AntManager.colonyPosition.y - position.y;
        dist = Mathf.Sqrt(dx * dx + dy * dy);
        inwardOrOutward *= 1f - Mathf.Clamp01(dist / pushRadius);
        vx += dx / dist * inwardOrOutward;
        vy += dy / dist * inwardOrOutward;

        if (ovx != vx || ovy != vy)
        {
            facingAngle = Mathf.Atan2(vy, vx);
        }
        float excitement = .3f;
        if (holdingResource)
        {
            excitement = 1f;
        }
        excitement *= speed / targetSpeed;
        DropPheromones(excitement);

        Matrix4x4 matrix = GetRotationMatrix(facingAngle);
        matrix.m03 = position.x / AntManager.mapSize;
        matrix.m13 = position.y / AntManager.mapSize;
        AntManager.Instance.matrices[i / instancesPerBatch][i % instancesPerBatch] = matrix;

        transform.position = new Vector3(position.x, position.y, 0) * mapScale;
        transform.rotation = Quaternion.Euler(0, 0, facingAngle * Mathf.Rad2Deg);

        if (holdingResource)
        {
            GetComponent<MeshRenderer>().material.color = yellowColor;
        }
        else
        {
            GetComponent<MeshRenderer>().material.color = redColor;
        }
    }



}
