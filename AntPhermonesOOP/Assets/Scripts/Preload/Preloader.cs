using System;
using System.Collections;
using System.Collections.Generic;
using Tayx.Graphy;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Tayx.Graphy.Utils.NumString;
using Newtonsoft.Json.Linq;

public class Preloader : SingletonPersistance<Preloader>
{
    public string TittleOne => "OOP-1-" + DateTime.Now.ToString("yyyy-MM-ddTHH-mm-ss");
    public string TittleTwo => "OOP-2-" + DateTime.Now.ToString("yyyy-MM-ddTHH-mm-ss");

    public Button normalSpeedBtn, fastSpeedBtn, loadBtn,recordBtn, recordBtn2, stopRecordBtn;
    public Text loadText;
    public InputField numberOfAntInput, mapInput, inveterInput;

    public GraphyManager graphyManager;

    public int speed;

    public Coroutine recordCoroutine;

    protected override void Awake()
    {
        base.Awake();
        if (_isInit)
        {
            Application.targetFrameRate = 60;
            StartCoroutine(DoActionOnAwake(null));
        }

    }

    public override IEnumerator DoActionOnAwake(Action onCompleted = null)
    {
        normalSpeedBtn.onClick.AddListener(() => { speed = 1; Time.timeScale = speed; });
        fastSpeedBtn.onClick.AddListener(() => { speed = 5; Time.timeScale = speed; });

        loadBtn.onClick.AddListener(() =>
        {
            if (CurrentActiveScene == "Preloader")
            {
                LoadSimulation();
            }
            else
            {
                UnLoadSimulation();
            }
        });

        stopRecordBtn.onClick.AddListener(() =>
        {
            StopRecord();
        });
        recordBtn.onClick.AddListener(() =>
        {
            stopRecordBtn.gameObject.SetActive(true);
            if (recordCoroutine == null)
            {
                recordCoroutine = StartCoroutine(RecordOneSetting());
            }
            else
            {
                StopCoroutine(recordCoroutine);
                recordCoroutine = null;
            }
        });

        recordBtn2.onClick.AddListener(() =>
        {
            stopRecordBtn.gameObject.SetActive(true);
            if (recordCoroutine == null)
            {
                recordCoroutine = StartCoroutine(RecordTwoSetting());
            }
            else
            {
                StopCoroutine(recordCoroutine);
                recordCoroutine = null;
            }
        });

        numberOfAntInput.text = "100";
        mapInput.text = "128";
        loadBtn.gameObject.SetActive(true);
        loadText.text = "Go to simulation";
        stopRecordBtn.gameObject.SetActive(false);
       
        yield return null;


    }
    private IEnumerator RecordOneSetting()
    {
        var interval = 0.2f;
        float time = 0;
        var writer = new SimpleCSVFileWriter(TittleOne, "Time", "NumberOfAnt", "MapSize", "ReservedMem", "AllocatedMem", "MonoMem","FPS","FrameTime", "Battery");
        while (true)
        {
            yield return new WaitForSeconds(interval);
            time += interval;
            var data = $"{time},{numberOfAntInput.text},{mapInput.text},{graphyManager.ReservedRam},{graphyManager.AllocatedRam},{graphyManager.MonoRam}, {graphyManager.CurrentFPS},{1000 / graphyManager.CurrentFPS},{SystemInfo.batteryLevel}";
            writer.AddNewLine(data);
        }
        recordCoroutine = null;
    }

    private IEnumerator RecordTwoSetting()
    {
        var interval = 500;
        if (int.TryParse(inveterInput.text, out var number))
        {
            interval = number;
        }
        else
        {
            inveterInput.text = interval.ToString();
        }

        Tayx.Graphy.Fps.G_FpsMonitor moniter = graphyManager.FpsMonitor;
        var writer = new SimpleCSVFileWriter(TittleTwo, "NumberOfAnt", "MapSize", "ReservedMem", "AllocatedMem", "MonoMem", "FPS", "FrameTime", "Battery");
        int maxFrameWait = 60;
        while (true)
        {
            var currentFrame = 0;
            float frameCount = 0;


            LoadSimulation();
            yield return new WaitForSeconds(1);
            while (currentFrame < maxFrameWait)
            {
                var framePerSec = 1 / Time.deltaTime;
                frameCount += framePerSec;
                currentFrame++;
                yield return null;
            }
            var average = frameCount / currentFrame;


            var data = $"{numberOfAntInput.text},{mapInput.text},{graphyManager.ReservedRam},{graphyManager.AllocatedRam},{graphyManager.MonoRam}, {average},{1000 / average},{SystemInfo.batteryLevel}";
            writer.AddNewLine(data);
            yield return new WaitForSeconds(1);
            UnLoadSimulation();


            if (!int.TryParse(numberOfAntInput.text, out number))
            {
                number = 1000;
            }

            numberOfAntInput.text = number + interval + "";
            yield return new WaitForSeconds(1);




        }
        recordCoroutine = null;
    }
   
    private void StopRecord()
    {
        if(recordCoroutine != null)
        {
            StopCoroutine(recordCoroutine);
            recordCoroutine = null;
        }
        stopRecordBtn.gameObject.SetActive(false);
    }

    public void LoadSimulation()
    {
        LoadScene("Simulation", null);
        loadText.text = "Back";


    }

    public void UnLoadSimulation()
    {
        LoadScene("Preloader", null);
        loadText.text = "Go to simulation";
    }



    public string CurrentActiveScene => SceneManager.GetActiveScene().name;

    public string SceneName => SceneManager.GetActiveScene().name;

    public void LoadScene(string level, System.Action onLoadSceneCompleted)
    {
        StartCoroutine(LoadSceneRoutineCoroutine(level, onLoadSceneCompleted));
    }
    private static IEnumerator LoadSceneRoutineCoroutine(string level, System.Action onLoadSceneCompleted = null)
    {
        string name = level;
        Application.backgroundLoadingPriority = ThreadPriority.High;
        var asyncLoad = SceneManager.LoadSceneAsync(name, LoadSceneMode.Single);

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        yield return null;
        onLoadSceneCompleted?.Invoke();
    }

    public void UnLoadScene(System.Action onLoadSceneCompleted)
    {
        StartCoroutine(UnLoadSceneRoutineCoroutine(onLoadSceneCompleted));
    }
    private static IEnumerator UnLoadSceneRoutineCoroutine(System.Action onLoadSceneCompleted = null)
    {
        string name = SceneManager.GetActiveScene().name;
        Application.backgroundLoadingPriority = ThreadPriority.High;
        var asyncLoad = SceneManager.UnloadSceneAsync(name);

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        yield return null;
        onLoadSceneCompleted?.Invoke();
    }
}
