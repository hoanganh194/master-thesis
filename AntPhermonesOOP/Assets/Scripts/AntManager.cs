﻿using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using UnityEngine;
using Color = UnityEngine.Color;

public class AntManager : MonoBehaviour
{
    public Ant antPrefab;


    public Material basePheromoneMaterial;
    public Renderer pheromoneRenderer;
    public Material antMaterial;
    public Material obstacleMaterial;
    public Material resourceMaterial;
    public Material colonyMaterial;
    public Mesh obstacleMesh;
    public Mesh colonyMesh;
    public Mesh resourceMesh;
    public Color searchColor;
    public Color carryColor;
    public int antCount;
    public int mapSize = 128;
    public int bucketResolution;
    public Vector3 antSize;
    public float antSpeed;
    [Range(0f, 1f)]
    public float antAccel;
    public float trailAddSpeed;
    [Range(0f, 1f)]
    public float trailDecay;
    public float randomSteering;
    public float pheromoneSteerStrength;
    public float wallSteerStrength;
    public float goalSteerStrength;
    public float outwardStrength;
    public float inwardStrength;



    public int obstacleRingCount;
    [Range(0f, 1f)]
    public float obstaclesPerRing;
    public float obstacleRadius;

    Texture2D pheromoneTexture;
    Material myPheromoneMaterial;

    public Color[] pheromones;
    Ant[] ants;
    public Matrix4x4[][] matrices;
    public Vector4[][] antColors;
    MaterialPropertyBlock[] matProps;
    Obstacle[] obstacles;
    Matrix4x4[][] obstacleMatrices;
    Obstacle[,][] obstacleBuckets;

    Matrix4x4 resourceMatrix;
    Matrix4x4 colonyMatrix;

    public Vector2 resourcePosition;
    public Vector2 colonyPosition;

    const int instancesPerBatch = 1023;

    public static AntManager Instance;
    void Awake()
    {
        Instance = this;
    }


    int PheromoneIndex(int x, int y)
    {
        return x + y * mapSize;
    }



    void GenerateObstacles()
    {
        List<Obstacle> output = new List<Obstacle>();
        for (int i = 1; i <= obstacleRingCount; i++)
        {
            float ringRadius = (i / (obstacleRingCount + 1f)) * (mapSize * .5f);
            float circumference = ringRadius * 2f * Mathf.PI;
            int maxCount = Mathf.CeilToInt(circumference / (2f * obstacleRadius) * 2f);
            int offset = Random.Range(0, maxCount);
            int holeCount = Random.Range(1, 3);
            for (int j = 0; j < maxCount; j++)
            {
                float t = (float)j / maxCount;
                if ((t * holeCount) % 1f < obstaclesPerRing)
                {
                    float angle = (j + offset) / (float)maxCount * (2f * Mathf.PI);
                    Obstacle obstacle = new Obstacle
                    {
                        position = new Vector2(mapSize * .5f + Mathf.Cos(angle) * ringRadius, mapSize * .5f + Mathf.Sin(angle) * ringRadius),
                        radius = obstacleRadius
                    };
                    output.Add(obstacle);
                    //Debug.DrawRay(obstacle.position / mapSize,-Vector3.forward * .05f,Color.green,10000f);
                }
            }
        }

        obstacleMatrices = new Matrix4x4[Mathf.CeilToInt((float)output.Count / instancesPerBatch)][];
        for (int i = 0; i < obstacleMatrices.Length; i++)
        {
            obstacleMatrices[i] = new Matrix4x4[Mathf.Min(instancesPerBatch, output.Count - i * instancesPerBatch)];
            for (int j = 0; j < obstacleMatrices[i].Length; j++)
            {
                obstacleMatrices[i][j] = Matrix4x4.TRS(output[i * instancesPerBatch + j].position / mapSize, Quaternion.identity, new Vector3(obstacleRadius * 2f, obstacleRadius * 2f, 1f) / mapSize);
            }
        }

        obstacles = output.ToArray();

        List<Obstacle>[,] tempObstacleBuckets = new List<Obstacle>[bucketResolution, bucketResolution];

        for (int x = 0; x < bucketResolution; x++)
        {
            for (int y = 0; y < bucketResolution; y++)
            {
                tempObstacleBuckets[x, y] = new List<Obstacle>();
            }
        }

        for (int i = 0; i < obstacles.Length; i++)
        {
            Vector2 pos = obstacles[i].position;
            float radius = obstacles[i].radius;
            for (int x = Mathf.FloorToInt((pos.x - radius) / mapSize * bucketResolution); x <= Mathf.FloorToInt((pos.x + radius) / mapSize * bucketResolution); x++)
            {
                if (x < 0 || x >= bucketResolution)
                {
                    continue;
                }
                for (int y = Mathf.FloorToInt((pos.y - radius) / mapSize * bucketResolution); y <= Mathf.FloorToInt((pos.y + radius) / mapSize * bucketResolution); y++)
                {
                    if (y < 0 || y >= bucketResolution)
                    {
                        continue;
                    }
                    tempObstacleBuckets[x, y].Add(obstacles[i]);
                }
            }
        }

        obstacleBuckets = new Obstacle[bucketResolution, bucketResolution][];
        for (int x = 0; x < bucketResolution; x++)
        {
            for (int y = 0; y < bucketResolution; y++)
            {
                obstacleBuckets[x, y] = tempObstacleBuckets[x, y].ToArray();
            }
        }


    }

    Obstacle[] emptyBucket = new Obstacle[0];
    public Obstacle[] GetObstacleBucket(Vector2 pos)
    {
        return GetObstacleBucket(pos.x, pos.y);
    }
    public Obstacle[] GetObstacleBucket(float posX, float posY)
    {
        int x = (int)(posX / mapSize * bucketResolution);
        int y = (int)(posY / mapSize * bucketResolution);
        if (x < 0 || y < 0 || x >= bucketResolution || y >= bucketResolution)
        {
            return emptyBucket;
        }
        else
        {
            return obstacleBuckets[x, y];
        }
    }

    IEnumerator Start()
    {
        while (Preloader.Instance == null)
        {
            yield return null;
        }
        if (int.TryParse(Preloader.Instance.numberOfAntInput.text, out var number))
        {
            antCount = number;
        }
        else
        {
            Preloader.Instance.numberOfAntInput.text = antCount.ToString();
        }

        if (int.TryParse(Preloader.Instance.mapInput.text, out number))
        {
            mapSize = number;
        }
        else
        {
            Preloader.Instance.mapInput.text = mapSize.ToString();
        }




        GenerateObstacles();

        colonyPosition = Vector2.one * mapSize * .5f;
        colonyMatrix = Matrix4x4.TRS(colonyPosition / mapSize, Quaternion.identity, new Vector3(4f, 4f, .1f) / mapSize);
        float resourceAngle = Random.value * 2f * Mathf.PI;
        resourcePosition = Vector2.one * mapSize * .5f + new Vector2(Mathf.Cos(resourceAngle) * mapSize * .475f, Mathf.Sin(resourceAngle) * mapSize * .475f);
        resourceMatrix = Matrix4x4.TRS(resourcePosition / mapSize, Quaternion.identity, new Vector3(4f, 4f, .1f) / mapSize);

        pheromoneTexture = new Texture2D(mapSize, mapSize);
        pheromoneTexture.wrapMode = TextureWrapMode.Mirror;
        pheromones = new Color[mapSize * mapSize];
        myPheromoneMaterial = new Material(basePheromoneMaterial);
        myPheromoneMaterial.mainTexture = pheromoneTexture;
        pheromoneRenderer.sharedMaterial = myPheromoneMaterial;
        ants = new Ant[antCount];
        matrices = new Matrix4x4[Mathf.CeilToInt((float)antCount / instancesPerBatch)][];
        for (int i = 0; i < matrices.Length; i++)
        {
            if (i < matrices.Length - 1)
            {
                matrices[i] = new Matrix4x4[instancesPerBatch];
            }
            else
            {
                matrices[i] = new Matrix4x4[antCount - i * instancesPerBatch];
            }
        }
        matProps = new MaterialPropertyBlock[matrices.Length];
        antColors = new Vector4[matrices.Length][];
        for (int i = 0; i < matProps.Length; i++)
        {
            antColors[i] = new Vector4[matrices[i].Length];
            matProps[i] = new MaterialPropertyBlock();
        }

        int batch = 40;
        int start = 0;

        var id = new List<int>();
        while (start < batch)
        {
            for (int i = start; i < antCount; i += batch)
            {
                var ant = Instantiate(antPrefab);
                ant.SetUp ( new Vector3(Random.Range(-5f, 5f) + mapSize * .5f, Random.Range(-5f, 5f) + mapSize * .5f, 0f));
                ant.pheromoneSteerStrength = pheromoneSteerStrength;
                ant.wallSteerStrength = wallSteerStrength;
                ant.goalSteerStrength = goalSteerStrength;
                ant.antAccel = antAccel;
                ant.trailAddSpeed = trailAddSpeed;
                ant.antSpeed = antSpeed;
                ant.randomSteering = randomSteering;

                ants[i] = ant;
                id.Add(i);
            }
            start++;
        }

        Debug.LogError($"Estimated size of ant: {Marshal.SizeOf(ants[0])} bytes");
        Debug.LogError($"Index: {string.Join(" ",id)}");
        Debug.LogError($"Count: {id.Count}");
        
        
        //for (int i = 0; i < antCount; i ++)
        //{
        //      ants[i] = new Ant(new Vector2(Random.Range(-5f, 5f) + mapSize * .5f, Random.Range(-5f, 5f) + mapSize * .5f))
        //      {
        //            trailAddSpeed = trailAddSpeed,
        //            antSpeed = antSpeed,
        //            randomSteering = randomSteering,
        //            pheromoneSteerStrength = pheromoneSteerStrength,
        //            wallSteerStrength = wallSteerStrength,
        //            goalSteerStrength = goalSteerStrength,
        //            antAccel = antAccel
        //      };
        //}

        Ant.rotationMatrixLookup = new Matrix4x4[Ant.rotationResolution];
        for (int i = 0; i < Ant.rotationResolution; i++)
        {
            float angle = (float)i / Ant.rotationResolution;
            angle *= 360f;
            Ant.rotationMatrixLookup[i] = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0f, 0f, angle), antSize);
        }
    }

    void FixedUpdate()
    {
        for (int i = 0; i < ants.Length; i++)
        {
            Ant ant = ants[i];
            ant.AntFixedUpdate(i, instancesPerBatch);
        }

        for (int x = 0; x < mapSize; x++)
        {
            for (int y = 0; y < mapSize; y++)
            {
                int index = PheromoneIndex(x, y);
                pheromones[index].r *= trailDecay;
            }
        }

        pheromoneTexture.SetPixels(pheromones);
        pheromoneTexture.Apply();

        for (int i = 0; i < matProps.Length; i++)
        {
            matProps[i].SetVectorArray("_Color", antColors[i]);
        }
    }
    private void Update()
    {
        for (int i = 0; i < obstacleMatrices.Length; i++)
        {
            Graphics.DrawMeshInstanced(obstacleMesh, 0, obstacleMaterial, obstacleMatrices[i]);
        }

        Graphics.DrawMesh(colonyMesh, colonyMatrix, colonyMaterial, 0);
        Graphics.DrawMesh(resourceMesh, resourceMatrix, resourceMaterial, 0);
    }
}
